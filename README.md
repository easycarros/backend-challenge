# Backend Challenge

Desafio Easy Carros para backend developers.

## Introdução

Você está participando do processo para integrar o time de Produto e Tecnologia da [Easy Carros](https://easycarros.com/).

Este desafio tem como objetivo avaliar seus *skills* na criação de código para backend -- mais especificamente, uma Web API -- para um problema do mundo real.

## O que é a Easy Carros?

A Easy Carros surgiu como uma plataforma de marketplace para serviços automotivos.

O modelo de negócios é parecido com o do Mercado Livre ou do Uber: nós fornecemos a tecnologia que une o consumidor -- pessoa ou empresa que possua veículos automotivos -- com empreendedores independentes, especializados na prestação de serviços como: lavagem a seco, troca de óleo, martelinho de ouro, etc.

## O desafio

Sua missão é criar uma versão simplificada do Easy Carros Marketplace, que funcione como uma Web API.

Você tem uma lista com os dados dos nossos parceiros prestadores de serviço -- `partners.json` -- contendo os seguintes dados:

- `id : String`: o ID do parceiro.
- `name : String`: o nome do parceiro.
- `availableServices : String[]`: um array contendo quais serviços aquele parceiro oferece. 
    - São apenas 2 serviços possíveis: `'OIL_CHANGE'` e `'DRY_WASHING'`. 
    - Cada parceiro pode oferecer apenas um deles ou ambos.
- `location : Object`: um objeto contendo as coordenadas do endereço-base do parceiro.
- `location.lat : Number`: a latitude da localização. Ex.: `-23.550520`
- `location.long : Number`: a latitude da localização. Ex.: `-46.633308`

Existem outras propriedades dentro do objeto `location`, mas elas não são importantes para este exercício.

Você tem uma lista com os dados dos nossos clientes -- `users.json` -- contendo os seguintes dados:

- `id : String` o ID do cliente.
- `name : String` o nome do cliente.
- `email : String` o email do cliente.
- `password : String` a senha do cliente em plain text (calma, é só pra facilitar, nós não fazemos isso!!! 😅).

Você precisa implementar as seguintes histórias de usuário:

### 1. Usuário se autentica na aplicação

> Eu, como cliente da Easy Carros, gostaria de me autenticar no sistema, fornecendo:
>
>   1. E-mail
>   2. Senha
>
> Caso o e-mail e a senha estejam corretos:  
>   quero receber um token de autenticação. 
>
> Caso contrário:  
>   quero receber uma mensagem de erro me informando do problema.

Dicas:

- Utilize JWT para não ter que se preocupar em como gerar o token.

### 2. Usuário solicita serviço

> Eu, como cliente da Easy Carros, gostaria de  
>   uma vez que estiver logado no sistema  
>   poder solicitar um dos serviços disponíveis, fornecendo:
>
>   1. O token de autenticação
>   2. O tipo de serviço que eu preciso: `'OIL_CHANGE'` ou `'DRY_WASHING'`.
>   3. As coordenadas (lat e long) do endereço onde quero que o serviço seja realizado.
> 
> Caso o token seja inválido:  
>   quero receber um erro me informando do problema.
>
> Caso haja algum profissional disponível para aquele serviço **dentro de um raio de 10 km**:  
>   quero receber as informações de 01 profissional escolhido (qualquer um que respeite os critérios). 
>
> Caso contrário:  
>   quero receber uma mensagem de erro me informando que não há um profissional disponível.

Dicas:

- Lembre-se de checar se o profissional fornece o tipo de serviço pedido.
- Você vai precisar converter a distância entre as coordenadas para quilômetros.

### Bônus

Implemente um segundo endpoint para a seguinte história de usuário:

### 3. Usuário busca profissionais disponíveis

> Eu, como cliente da Easy Carros, gostaria de,
>   uma vez que estiver logado no sistema,  
>   poder visualizar uma lista de profissionais que atendem um determinado endereço, fornecendo:
>
>   1. O token de autenticação
>   2. **O endereço** (Ex.: Av. Paulista, 1578) onde quero que o serviço seja realizado.
>
> Caso o token seja inválido:  
>   quero receber um erro me informando do problema.
>
> Caso haja profissionais disponíveis para aquele serviço **dentro de um raio de 10 km**:  
>   quero receber uma lista com todos os profissionais que atendem dentro desse raio, contendo pelo menos:
>
>   1. Nome
>   2. Serviços disponíveis
>
> Caso contrário:
>   quero receber uma lista vazia.

Dicas:

- Use algum serviço de geo-location (Ex.: Google Maps API) para obter as coordenadas a partir de um endereço.

## As regras do jogo

- 🌐 A API deve ser implementada utilizando REST ou GraphQL.
- 🚪 A API deve escutar a porta `8080`.
- 🈴 Use a linguagem com a qual você se sente mais confortável (aqui nós utilizamos principalmente **Node.js**, mas qualquer linguagem "mainstream" é bem vinda).
- 📚 Use qualquer solução para data storage: banco de dados, arquivos de texto, memória, etc.
- ✔️ Assuma que todos os dados enviados para a API são válidos (não há necessidade de uma camada de validação).
- 🚢 Envie seu código para um repositório público para leitura (Github, Bitbucket, Gitlab, etc.).
- 🗒️ Crie um arquivo `README` na raiz do projeto com instruções detalhadas de como executar seu código.

### Bônus

- 📑 Unit tests pelo menos para as regras de negócio.
- 📦 Use Docker para empacotar todas as dependências em um único lugar.

### Como vou ser avaliado?

Vamos analisar seu código com respeito a:

- Qualidadade de código
    - Keep it simple! (KISS)
- Boas práticas
    - Separation of Concerns (SoC)
    - Design patterns (se houver necessidade)
    - Clean code
- Code styling
    - Use um code linter 🙏

O que **NÃO** vamos analisar:

- Performance
- Escolha da tecnologia A em vez da B

## Para onde enviar seu repositório

Envie um email para `people@easycarros.com` com o assunto `Desafio Backend - [SEU NOME]` contendo o link para o repositório que você criou.
