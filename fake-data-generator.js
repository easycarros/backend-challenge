const { writeFileSync } = require('fs');
const cuid = require('cuid');
const faker = require('faker/locale/pt_BR');
const addresses = require('./addresses.json');
const availableServices = ['OIL_CHANGE', 'DRY_WASHING'];

const randomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;


const randomSubset = (list, subsetLength = randomInt(1, list.length)) => {
  if (subsetLength > list.length) {
    return randomSubset(list, list.length);
  }

  const subset = [...new Set(
    Array(subsetLength)
      .fill()
      .map(() => faker.random.arrayElement(list))
  )];

  return subset.length === subsetLength ? subset : randomSubset(list, subsetLength);
}

const generatePartners = count => Array(count)
  .fill()
  .map(() => ({
    id: cuid(),
    name: faker.name.firstName() + ' ' + faker.name.lastName(),
    location: faker.random.arrayElement(addresses),
    availableServices: randomSubset(availableServices),
  }));

const generateUsers = count => Array(count)
  .fill()
  .map(() => {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    return {
      id: cuid(),
      name: `${firstName} ${lastName}`,
      email: faker.internet.email(firstName, lastName),
      password: faker.internet.password(),
    }
  });

const partners = generatePartners(20);
const users = generateUsers(60);

writeFileSync('partners.json', JSON.stringify(partners, null, 2));
writeFileSync('users.json', JSON.stringify(users, null, 2));
